<?php
$countArticleHome = new actuality();

$countHome = $countArticleHome->countArticle();
$pagesHome = ceil($countHome->idCount / 3);

// On instancie un objet
$getActuality = new actuality();
// Si aucune page n'a été sélectionné
if (!isset($_GET['page'])) {
    // On en attribut une valeur par défaut
    $displayActuality = $getActuality->getArticle(0);
    // Si une page a été sélectionné,
} else if (isset($_GET['page'])) {
    if (filter_var($_GET['page'], FILTER_VALIDATE_INT)) {
        // on l'attribut à la méthode
        $displayActuality = $getActuality->getArticle($_GET['page']);
    } else {
        header('Location: Accueil');
    }
}
?>