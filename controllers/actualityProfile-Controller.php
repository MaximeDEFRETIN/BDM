<?php
// On instancie un objet
$actuality = new actuality();
// On crée un tableau dans lequel on met les messages destinés à l'utilisateur
$messageArticle = array();
// La regex sert à s'assurer que l'utilisateur entre les informations attendues
$regexText = '/[A-ZÉÈÀÊÀÙÎÏÜËa-zéèàêâùïüë0-9.\-\'~$£%*#{}()`ç+=œ“€\/:;,!]+/';

// On vérifie si l'utilisateur a cliquer sur le bouton submitArticle
if (isset($_POST['submitArticle'])) {
    // On vérifie que l'utilisateur à donner un titre à l'article et le texte de l'article
    if(empty($_POST['titleArticle']) && empty($_POST['article'])){
        // Si aucun titre et texte n'est donné, alors on envoie un message d'erreur
        $messageArticle['emptyFormArticle'] = 'Il faut remplir entièrement le formulaire pour publier un article.';
    } else {
    // On vérifie que les informations entrées dans le formulaire correspondent à celles qui sont attendues
    // Si les champs sont vides, on envoie un message d'erreur à chaque champ vide
    // Sinon on attribut à chaque attribut ce qui est entré dans le champ
    if (!empty($_POST['titleArticle'])) {
        if (preg_match($regexText, $_POST['titleArticle'])) {
            $actuality->title = htmlspecialchars(strip_tags($_POST['titleArticle']));
        } else {
            $messageArticle['titleWrong'] = 'Le titre n\'est pas correcte !';
        }
    } else {
        $messageArticle['emptyTitleArticle'] = 'Le titre n\'est pas donné.';
    }
    if (!empty($_POST['article'])) {
        if (preg_match($regexText, $_POST['titleArticle'])) {
            $actuality->article = htmlspecialchars(strip_tags($_POST['article']));
        } else {
            $messageArticle['textWrong'] = 'Le texte n\'est pas correcte !';
        }
    } else {
        $messageArticle['emptyArticle'] = 'L\'article n\'est pas donné.';
    }
    
    $actuality->id_agdjjg_user = $_SESSION['id'];
    
    // Si il n'y a aucune erreur, on ajoute un article
    if (count($messageArticle) == 0) {
        // On appelle la méthode
        $actuality->addActuality();
        
        $actuality->article = '';
        $actuality->title = '';
        $actuality->id_agdjjg_user = 0;
        
        // On affiche un messsage pour que l'utilisateur soit avertit de l'ajout de l'article
        $messageArticle['add'] = 'L\'article a été ajouté !';
        
        // On recharge la page au bout de 5 secondes
        header('refresh:5; url=Profile');
        }
    }
}

// On instancie un nouveau objet
$countArticle = new actuality();

// ON appel la méthode pour compter le nombre d'article dans la base dee donnée
$count = $countArticle->countArticle();
// On calcule le nombre de page pour la pagination des articles
$pages = ceil($count->idCount / 3);

// On récupère les articles
$getActuality = new actuality();

// Si il n'y a pas la variable $_GET['paPro'] ou qu'elle n'est pas inférieur à 0,
// la variable $page vaut automatiquement 0 et on affiche les 3 premiers articles
if (!isset($_GET['paPro']) || $_GET['paPro'] < 0) {
    $displayActualityProfile = $getActuality->getArticle(0);
// Si il y a bien une variable $_GET['paPro'],
// on attribut la variable $_GET['paPro'] à la variable $page
} else if (isset($_GET['paPro'])) {
    if (filter_var($_GET['paPro'], FILTER_VALIDATE_INT)) {
        $displayActualityProfile = $getActuality->getArticle($_GET['paPro']);
    } else {
        header('Location: Profile');
    }
}

// On instancie un nouveau objet
$deleteActuality = new actuality();
    
// Si un utilisateur clique sur un bouton supprimer
if (isset($_GET['delAct'])) {
    if (filter_var($_GET['delAct'], FILTER_VALIDATE_INT)) {
        // On récupère l'id de l'article
        $deleteActuality->id = $_GET['delAct'];
        // On appel une méthode pour supprimer l'article
        $deleteActuality->deleteActualityById();
        // Ensuite on redirige l'utilisateur vers la page profile
        header('Location: Profile');
    } else {
        header('Location: Profile');
    }
}