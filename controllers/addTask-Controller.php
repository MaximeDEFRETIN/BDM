<?php
// On crée un objet $task
$task = new task();
// On crée un tableau dans lequel on met les messages destinés aux utilisateurs
$messageTask = array();
$regexTask = '/[A-ZÉÈÀÊÀÙÎÏÜËa-zéèàêâùïüë0-9.\-\'~$£%*#{}()`ç+=œ“€\/:;,!]+/';

// Lorsque l'on clique sur submitTask et qu'il n'y a aucune erreur, le formulaire es validé
if (isset($_POST['submitTask'])) {
    if(empty($_POST['suggestedTask']) && empty($_POST['descriptionTask'])){
        $messageTask['emptyFormTask'] = 'Il faut remplir entièrement le formulaire ajouter une tâche.';
    } else {
    // On vérifie que les informations entrées dans le formulaire correspondent à celles qui sont attendues
    if (!empty($_POST['suggestedTask'])) {
        if (preg_match($regexTask, $_POST['suggestedTask'])) {
            $task->suggested_task = htmlspecialchars(strip_tags($_POST['suggestedTask']));
        } else {
            $messageTask['suggestedWrong'] = 'Le titre n\'est pas correcte !';
        }
    } else {
        $messageTask['emptySuggestedTask'] = 'Le nom n\'est pas donné.';
    }
    if (!empty($_POST['descriptionTask'])) {
        if (preg_match($regexTask, $_POST['descriptionTask'])) {
            $task->description_task = htmlspecialchars(strip_tags($_POST['descriptionTask']));
        } else {
            $messageTask['descriptionWrong'] = 'Le titre n\'est pas correcte !';
        }
    } else {
        $messageTask['emptyDescriptionTask'] = 'La description n\'est pas donnée.';
    }
    
    $task->id_agdjjg_user = $_SESSION['id'];

    // Si $task ne correspond pas au model, alors on envoie un message d'erreur
    if (count($messageTask) == 0) {
        // On appelle la méthode
        $task->addTask();

        $task->suggested_task = '';
        $task->description_task = '';
        $task->id_agdjjg_user = 0;
        
        $messageTask['add'] = 'La tâche a été ajoutée !';
        
        header('refresh:5; url=Profile');
        }
    }
}

// On crée un objet $task
$updateStatusTask = new task();
// On crée un tableau dans lequel on met les messages destinés aux utilisateurs
$messageStatusTask = array();

// Si l'utilisateur clique sur le bouton upStatus
if (isset($_GET['upStatus'])) {
    if (filter_var($_GET['upStatus'], FILTER_VALIDATE_INT)) {
        // On récupère l'id de la tâche en GET
        $updateStatusTask->id = $_GET['upStatus'];
        // On attribue un statut
        $updateStatusTask->status_task = 'Terminee';
        // Et on change le statut de la tâche
        $updateStatusTask->updateStatusTaskById();

        // On affiche un message à l'utilisateur
        $messageStatusTask['updateStatus'] = 'La tâche est bien terminée !';
        // On recharge la page au bout de 5 secondes
        header('refresh:5; url=Profile');
    } else {
        header('Location: Profile');
    }
}

// On crée un objet $task
$delTask = new task();

// Si l'utilisateur clique sur delTas
if (isset($_GET['delTas'])) {
    if (filter_var($_GET['delTas'], FILTER_VALIDATE_INT)) {
            $delTask->id = $_GET['delTas'];
            // On supprime la tâche
            $delTask->deleteTaskById();

            header('Location: Profile');
    } else {
        header('Location: Profile');
    }
}
?>