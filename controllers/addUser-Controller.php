<?php
if (isset($_POST['checkMail']) && filter_var($_POST['checkMail'], FILTER_VALIDATE_EMAIL)) {
    session_start();
    include_once '../models/dataBase.php';
    include_once '../models/user.php';
    $mailUnique = new user();
    $mailUnique->mail = htmlspecialchars(strip_tags($_POST['checkMail']));
    $checkMail = $mailUnique->checkMailUnique();
    // On vérifie que $checkMail est différent de false mais pas de 0 ou 1
    if ($checkMail !== false) {
        if ($checkMail == 1) {
            $_SESSION['formError'] = true;
        }
        echo json_encode($checkMail);
    }
} else {
    // On creée l'objet $user
    $user = new user();
    // Les regex servent à vérifier les information entrées dans le formulaire
    // La regex donne comme pattern n'importe quel lettre en majuscule comme 1ère lettre, suivis de n'importe quel lettre minuscule quel que soit leur nombre
    // avec un tiret possible et n'importe quelle lettre majuscule et n'importe quel lettre minuscule
    $regexName = '/^[A-ZÉÈÀÊÀÙÎÏÜË]{1}[a-zéèàêâùïüë]+[-]{0,}[A-ZÉÈÀÊÀÙÎÏÜË]{0,1}[a-zéèàêâùïüë]{0,}/';
    // Indique que n'importe quel caractère peut être choisis tant que le mot de passe a entre 4 et 8 caractères
    $regexPassword = '/^(.){4,8}$/';
    // $messageSignUp sert à stocker les erreurs
    $messageSignUp = array();
    // Lorsque l'on clique sur signIn et qu'il n'y a aucune erreur, le formulaire es validé
    if (isset($_POST['submitRegistrer'])) {
        if(empty($_POST['last_name']) && empty($_POST['first_name']) && empty($_POST['mail'])){
            $messageSignUp['emptyFormSignIn'] = 'Il faut remplir entièrement le formulaire pour s\'inscrire.';
        } else {
        // On vérifie que les informations entrées dans le formulaire correcpondent à celles qui sont attendues
        if (!empty($_POST['last_name'])) {
            $user->last_name = strip_tags($_POST['last_name']);
            if (!preg_match($regexName, $user->last_name)) {
                $messageSignUp['last_name'] = 'Le nom n\'est pas correct.';
            }
        } else {
            $messageSignUp['emptyLastName'] = 'Le nom n\'est pas donné.';
        }
        if (!empty($_POST['first_name'])) {
            $user->first_name = strip_tags($_POST['first_name']);
            if (!preg_match($regexName, $user->first_name)) {
                $messageSignUp['first_name'] = 'Le prenom n\'est pas correct.';
            }
        } else {
            $messageSignUp['emptyFirstName'] = 'Le prénom n\'est pas donné.';
        }
        if (!empty($_POST['mail'])) {
            if (filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
                $user->mail = $_POST['mail'];
            } else {
                $messageSignUp['mail'] = 'L\'adresse mail n\'est pas correcte';
            }
        } else {
            $messageSignUp['emptyMail'] = 'L\'addresse mail n\'est pas donné.';
        }

        $user->key_user = rand(1000, 9999);

        // Si $user ne correspond pas au model, alors on envoie un message d'erreur
        if (count($messageSignUp) == 0) {
            // On appelle la méthode
            $user->addUser();

            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . '\r\n';
            $headers .= 'From: Maxime <no-reply@bdm.fr>' . '\r\n';
            $to = $user->mail;
            $subject = 'Finalisation de l\'inscription.';
            $message = 'Bonjour !' . "\r\n" . 'Voici un <a href="bdm/Nouveau-mdp-'.$user->key_user.'">lien</a> te permettant de choisir ton mot de passe.';

            mail($to, $subject, $message,$headers); 

            $user->last_name = '';
            $user->first_name = '';
            $user->mail = '';
            $user->key_user = '';
            $messageSignUp['add'] = 'L\'inscription du nouveau bénévole est réussie !';
            
            header('refresh:5; url=Profile');
            }
        }
    }
}
?>