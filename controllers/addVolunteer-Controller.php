<?php
// On instancie un objet $volunteer
$volunteer = new task();
// On crée un tableau qui contiendra tous les messages destinés à l'utilisateur
$messageAddVolunteer = array();

// Lorsque l'on clique sur signIn et qu'il n'y a aucune erreur, le formulaire es validé
if (isset($_GET['addVo'])) {
    // On vérifie que $_GET['addVo'] soit bien un entier
    if (filter_var($_GET['addVo'], FILTER_VALIDATE_INT)) {
        $volunteer->id_task = strip_tags($_GET['addVo']);
        $volunteer->volunteer = $_SESSION['first_name'] . ' ' . $_SESSION['last_name'];
        $volunteer->id = $_SESSION['id'];
        $volunteer->assignVolunteer();
        
        $volunteer->id = strip_tags($_GET['addVo']);
        $volunteer->status_task = 'En cours';
        $volunteer->updateStatusTaskById();

        // On affiche un message pour l'utilisateur
        $messageAddVolunteer['messageAddVolunteer'] = 'Tu as bien été ajouté.';

        // On recharge la page
        header('refresh:5; url=Profile');
    } else {
        header('Location: Profile');
    }
}

// On instancie un objet
$assignedVolunter = new task();

// Si il y a une session, on affiche les volontaires pour chaque tâche
if (isset($_SESSION['id'])) {
    $displayAssignedVolunter = $assignedVolunter->getAssignedVolunteer();
//    $getIdVolunteeer = $assignedVolunter->getIdAssignedVolunteer();
}