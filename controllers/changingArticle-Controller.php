<?php
// On instancie un objet
$changeArticle = new actuality();

// Si l'utilisateur veut modifier un article,
// on le récupère grâce à son id
if (isset($_GET['chanArt'])) {
    if (filter_var($_GET['chanArt'], FILTER_VALIDATE_INT)) {
        $changeArticle->id = $_GET['chanArt'];
        $displayActuality = $changeArticle->getArticleById();
    } else {
        header('Location: Profile');
    }
}

// On instancie un objet
$updateArticle = new actuality();
$insertSuccessUpdateArticle = false;
// On crée une variable contenant tous les messages destinés à l'utilisateur
$messageChangingActuality = array();
$regexUpdateEvent = '/[A-ZÉÈÀÊÀÙÎÏÜËa-zéèàêâùïüë0-9.\-\'~$£%*#{}()`ç+=œ“€\/:;,!]+/';

if (isset($_POST['submitUpdateArticle'])) {
    if (empty($_POST['updateTitle']) || empty($_POST['updateText'])) {
        $messageChangingActuality['emptyFormChanging'] = 'Il y a un champ vide !';
    } else {
        if (!empty($_POST['updateTitle'])) {
            if (preg_match($regexEvent, $_POST['updateTitle'])) {
                $updateArticle->title = htmlspecialchars(strip_tags($_POST['updateTitle']));
            } else {
                $messageChangingActuality['titleUpWrong'] = 'Le titre n\'est pas correcte !';
            }
        } else {
            $messageChangingActuality['noUpdateTitle'] = 'Aucun titre n\'a été donné.';
        }

        if (!empty($_POST['updateText'])) {
            if (preg_match($regexEvent, $_POST['updateText'])) {
                $updateArticle->article = htmlspecialchars(strip_tags($_POST['updateText']));
            } else {
                $messageChangingActuality['articleUpWrong'] = 'Le titre n\'est pas correcte !';
            }
        } else {
            $messageChangingActuality['noUpdateText'] = 'Aucun texte n\'a été donné.';
        }

        if (count($messageChangingActuality) === 0) {
            $updateArticle->id = $_GET['chanArt'];
            if (!$updateArticle->updateArticleById()) {
                $messageChangingActuality['noUpdateArticle'] = 'L\'article n\'a pas pu être mis à jour !';
            } else {
                $insertSuccessUpdateArticle = true;
                $messageChangingActuality['updateArticle'] = 'L\article a été mis à jour !';
                
                $updateArticle->id = 0;
                $updateArticle->title = '';
                $updateArticle->article = '';
            }
        }
    }
}
?>