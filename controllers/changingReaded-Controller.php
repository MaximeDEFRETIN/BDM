<?php
// On crée un objet $readed
$updateReaded = new bookReaded();
// On crée un tableau dans lequel on met les messages destinés aux utilisateurs
$messageUpReaded = array();

// Si l'utilisateur clique sur le bouton $_GET['upRea']
if (isset($_POST['submitUpDescription'])) {
    if (isset($_GET['upRea'])) {
        if (filter_var($_GET['upRea'], FILTER_VALIDATE_INT)) {
            // On récupère l'id de la tâche en GET
            $updateReaded->id = $_GET['upRea'];
            $updateReaded->title = $_POST['upTitle'];
            $updateReaded->opinion_book = $_POST['upDescriptionBook'];

            $updateReaded->updateReadedBook();

            // On affiche un message à l'utilisateur
            $messageUpReaded['updateReaded'] = 'La description est modifiée !';
            // On recharge la page au bout de 5 secondes
            header('refresh:5; url=Profile');
        } else {
            header('Location: Profile');
        }
    }
}
?>