<?php
// Si il y a bien une session, on affiche les tâches
if (isset($_SESSION['id'])) {
    $getTask = new task();
    $displayTask = $getTask->getTask();
}

// On crée un tableau qui permettra d'afficher des messages pour l'utilisateur
$messageDeleteVolunteerTask = array();
// Si il y a bien une session, on affiche les tâches
if (isset($_SESSION['id'])) {
    if (isset($_GET['idTas'])) {
        if (filter_var($_GET['idTas'], FILTER_VALIDATE_INT)) {
            // On instancie un nouveau objet
            $deleteVolunteer = new task();

            // On attribut des valeurs
            $deleteVolunteer->id = $_SESSION['id'];
            $deleteVolunteer->id_agdjjg_task = $_GET['idTas'];
            $deleteVolunteer->deleteVolunteerById();
            
            // On affiche un message pour l'utilisateur
            $messageDeleteVolunteerTask['deleteVolunteer'] = 'Tu n\'es plus dans la liste !';
            
            header('refresh:5; url=Profile');
        } else {
            header('Location: Profile');
        }
    }
}
?>