<?php
// On instancie un objet
$event = new event_association();
// On crée une variable servant à stocker les messages d'erreurs
$messageEvent = array();
// La regex sert à s'assurer que l'utilisateur entre les informations attendues
$regexEvent = '/[A-ZÉÈÀÊÀÙÎÏÜËa-zéèàêâùïüë0-9.\-\'~$£%*#{}()`ç+=œ“€\/:;,!]+/';

// Si l'utilisateur clique sur le bouton submitEvent
if (isset($_POST['submitEvent'])) {
    // On vérifie que le formulaire ne soit pas vide
    if(empty($_POST['descriptionEvent']) && empty($_POST['suggestedEvent']) && empty($_POST['statusEvent']) && empty($_POST['dateEvent'])){
            $messageEvent['emptyFormEvent'] = 'Il faut remplir entièrement le formulaire ajouter un évènnement.';
    } else {
        // On vérifie que les informations entrées dans le formulaire correcpondent à celles qui sont attendues
        if (!empty($_POST['descriptionEvent'])) {
            if (preg_match($regexEvent, $_POST['descriptionEvent'])) {
                $event->description_event = htmlspecialchars(strip_tags($_POST['descriptionEvent']));
            } else {
                $messageEvent['titleWrong'] = 'Le titre n\'est pas correcte !';
            }
        } else {
            $messageEvent['emptyDescriptionEvent'] = 'Tu n\'as pas décrit l\'évènnement.';
        }
        if (!empty($_POST['suggestedEvent'])) {
            if (preg_match($regexEvent, $_POST['suggestedEvent'])) {
                $event->suggested_event = htmlspecialchars(strip_tags($_POST['suggestedEvent']));
            } else {
                $messageEvent['titleWrong'] = 'Le titre n\'est pas correcte !';
            }
        } else {
            $messageEvent['noStatusEvent'] = 'Tu n\as pas donné de nom à l\'évènnement.';
        }
        if (!empty($_POST['statusEvent'])) {
            if ($_POST['statusEvent'] === 'Formation' || 'Réunion' || 'Entre bénévole' || 'Sortie' || 'Autres' ) {
                $event->status_event = htmlspecialchars(strip_tags($_POST['statusEvent']));
            }
        } else {
            $messageEvent['noStatusEvent'] = 'Le statut de l\'évènnement  n\'est pas donnée.';
        }
        if (!empty($_POST['dateEvent'])) {
            $event->date_event = $_POST['dateEvent'];
        } else {
            $messageEvent['noDateEvent'] = 'La date de l\'évènnement  n\'est pas donnée.';
        }
        
        // On vérifie que le fichier bien été envoyé et qu'il n'y a pas d'erreur
        if (!empty($_FILES['documentFileEvent']['name']) && $_FILES['documentFileEvent']['error'] == 0) {
            // Testons si le fichier n'est pas trop gros
            if ($_FILES['documentFileEvent']['size'] <= 5000000) {
    
                // On vérifie si le fichier est dans un des formats autorisés
                $fileType = pathinfo($_FILES['documentFileEvent']['name']);
                $extension_upload = $fileType['extension'];
                $extensions_autorisees = array('pdf');
                $contentType = $_FILES['documentFileEvent']['type'];
                $contentType_autorisees = array('application/pdf');
                if (in_array($extension_upload, $extensions_autorisees)) {
                    if (in_array($contentType, $contentType_autorisees)) {
                        // On supprime certains caractères
                        $notAccepted = array(" ", "/", ",", "?", "/", "`", "'", "~", "_", "-", "\\", "\"", "<", ">", "?", "&");
                        $name = str_replace($notAccepted, "", $_FILES['documentFileEvent']['name']);
                        move_uploaded_file($_FILES['documentFileEvent']['tmp_name'], 'document/' . $name );
                        // On modifie les permissions pour limiter au maximum les droits
                        chmod('document/' . $name, 0660);
                        //On récupère l'id de l'utilisateur pour l'associer au chemin du fichier
                        $event->path_document_event = 'document/' . $name;
                    } else {
                        $messageEvent['contentFile'] = 'Le contenu du fichier n\'est pas autorisée.';
                    }
                } else {
                    $messageEvent['extensionFile'] = 'L\'extension du fichier n\'est pas autorisée.';
                }
            } else {
                $messageEvent['sizeFile'] = 'Le fichier est trop lourd.';
            }
        }

        $event->author_event = $_SESSION['last_name'] . ' ' . $_SESSION['first_name'];
        $event->id_agdjjg_user = $_SESSION['id'];
        
        // Si $event ne correspond pas au model, alors on envoie un message d'erreur
        if (count($messageEvent) == 0) {
                // On appelle la méthode pour ajouter l'évènement
                $event->insertEventAssociation();

                $event->description_event = '';
                $event->suggested_event = '';
                $event->status_event = '';
                $event->author_event = '';
                $event->id_agdjjg_user = '';
                $event->id_agdjjg_status_event = '';
                $event->path_document_event = '';

                $messageEvent['add'] = 'L\'évènement est bien ajouté !';
                
                header('refresh:5; url=Profile');
            }
        }
}

// On instancie un objet
$getEvent = new event_association();

if (isset($_SESSION['id'])) {
    // On récupère les évènements pour les afficher
    $displayEvent = $getEvent->getEvent();
}

// On instancie un objet
$addVolunteerEvent = new event_association();
// On crée une variable servant à stocker les messages d'erreurs
$messageVolunteerEvent = array();

// Si l'utilisateur clique le bouton addPar
if (isset($_GET['addPar'])) {
    if (filter_var($_GET['addPar'], FILTER_VALIDATE_INT)) {
        // On récupère les informations sur le bénévoles
        $addVolunteerEvent->id = $_SESSION['id'];
        $addVolunteerEvent->id_agdjjg_event_association = $_GET['addPar'];
        $addVolunteerEvent->volunteer_present =  $_SESSION['last_name'] . ' ' . $_SESSION['first_name'];
        // On appele la méthode pour ajouter le bénévole à un évènement
        $addVolunteerEvent->addVolunteerEvent();

        // Le message sert à informer l'utilisateur de l'enregistrement de sa participation
        $messageVolunteerEvent['addVolunteerEvent'] = 'Ta participation as bien été enregistré !';
        // On recharge la page au bout de 5 secondes
        header('refresh:5; url=Profile');
    } else {
        header('Location: Profile');
    }
}

// On instancie un objet
$getVolunteerEvent = new event_association();

// Si il y a une session, on récupère les bénévoles participant à un évènement
if ($_SESSION['id']) {
    $displayVolunteerEvent = $getVolunteerEvent->getVolunteerEvent();
}

// On crée un tableau qui permettra d'afficher des messages pour l'utilisateur
$messageDeletVolunteerEvent = array();
// Si il y a bien une session, on affiche les tâches
if (isset($_SESSION['id'])) {
    if  (isset($_GET['idEvent'])) {
        if (filter_var($_GET['idEvent'], FILTER_VALIDATE_INT)) {
            // On instancie un objet
            $deleteVolunteerEvent = new event_association();

            $deleteVolunteerEvent->id = $_SESSION['id'];
            $deleteVolunteerEvent->id_agdjjg_event_association = $_GET['idEvent'];
            $deleteVolunteerEvent->deleteVolunteerEventById();
            $messageDeletVolunteerEvent['deleteVolunteer'] = 'Tu n\'es plus dans la liste !';
            
            // On redirige l’utilisateur au bout de 5 secondes
            header('refresh:5; url=Profile');
        } else {
            header('Location: Profile');
        }
    }
}

// On instancie un objet
$deleteEvent = new event_association();

// Si l'utilisateur clique sur le bouton supprimer
if (isset($_GET['delEv'])) {
    // On vérifie que la valeur récupéré soit bien un entier, sinon on redirige vers la pae profile
    if (filter_var($_GET['delEv'], FILTER_VALIDATE_INT)) {
        // On récupère l'id de l'évènement
        $deleteEvent->id = $_GET['delEv'];
        // On applique la suppresion de fichier à tous les fichier ayant le format PDF
        unlink('document/' . $_GET['path']);

        // On appel la méthode pour supprimer l'évènement
        $deleteEvent->deleteEventAssociation();
        
        // On redirige ensuite l'utilisateur vers la page profile
        header('Location: Profile');
    }
}
?>