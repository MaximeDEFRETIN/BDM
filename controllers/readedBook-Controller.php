<?php
// On crée un objet $readed
$readed = new bookReaded();
// On crée un tableau dans lequel on met les messages destinés aux utilisateurs
$messageReaded = array();
$regexReaded = '/[A-ZÉÈÀÊÀÙÎÏÜËa-zéèàêâùïüë0-9.\-\'~$£%*#{}()`ç+=œ“€\/:;,!]+/';

// Lorsque l'on clique sur submitTask et qu'il n'y a aucune erreur, le formulaire es validé
if (isset($_POST['submitDescription'])) {
    if(empty($_POST['title']) && empty($_POST['descriptionBook'])){
        $messageReaded['emptyFormReaded'] = 'Il faut remplir entièrement le formulaire.';
    } else {
    // On vérifie que les informations entrées dans le formulaire correspondent à celles qui sont attendues
    if (!empty($_POST['title'])) {
        if (preg_match($regexTask, $_POST['title'])) {
            $readed->title = htmlspecialchars(strip_tags($_POST['title']));
        } else {
            $messageReaded['titleWrong'] = 'Le titre n\'est pas correcte !';
        }
    } else {
        $messageReaded['emptyTitle'] = 'Le titre n\'est pas donné.';
    }
    if (!empty($_POST['descriptionBook'])) {
        if (preg_match($regexReaded, $_POST['descriptionBook'])) {
            $readed->opinion_book = htmlspecialchars(strip_tags($_POST['descriptionBook']));
        } else {
            $messageReaded['descriptionWrong'] = 'La description n\'est pas correcte !';
        }
    } else {
        $messageReaded['emptyDescriptionReaded'] = 'La description n\'est pas donnée.';
    }
    
    $readed->mail= $_SESSION['mail'];

    // Si $readed ne correspond pas au model, alors on envoie un message d'erreur
    if (count($messageReaded) == 0) {
        // On appelle la méthode
        $readed->addBookReaded();

        $readed->title = '';
        $readed->opinion_book = '';
        $readed->mail= '';
        
        $messageReaded['add'] = 'La description du livre a été ajouté !';
        
        header('refresh:5; url=Profile');
        }
    }
}

// On instancie un nouveau objet
$countReadedBook = new bookReaded();

// On appel la méthode pour compter le nombre d'article dans la base dee donnée
$count = $countReadedBook->countReadedBook();
// On calcule le nombre de page pour la pagination des articles
$pagesBook = ceil($count->id_count / 3);

// On récupère les articles
$getReadedBook = new bookReaded();

// Si il n'y a pas la variable $_GET['paRea'] ou qu'elle n'est pas inférieur à 0,
// la variable vaut automatiquement 0 et on affiche les 3 premiers articles
if (!isset($_GET['paRea']) || $_GET['paRea'] < 0) {
    $displayReadedBook = $getReadedBook->getReadedBook(0);
// Si il y a bien une variable $_GET['paRea'],
// on attribut la valeur de $_GET['paRea'] à la variable $page
} else if (isset($_GET['paRea'])) {
    if (filter_var($_GET['paRea'], FILTER_VALIDATE_INT)) {
        $displayReadedBook = $getReadedBook->getReadedBook($_GET['paRea']);
    } else {
        header('Location: Profile');
    }
}

// On crée un objet $readed
$delReaded = new bookReaded();

// Si l'utilisateur clique sur delRea
if (isset($_GET['delRea'])) {
    if (filter_var($_GET['delRea'], FILTER_VALIDATE_INT)) {
            $delReaded->id = $_GET['delRea'];
            // On supprime la description du livre
            $delReaded->deleteReadedBookById();

            header('Location: Profile');
    } else {
        header('Location: Profile');
    }
}