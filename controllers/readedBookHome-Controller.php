<?php
// On instancie un nouveau objet
$countReadedBookHome = new bookReaded();

// On appel la méthode pour compter le nombre d'article dans la base dee donnée
$countHome = $countReadedBookHome->countReadedBook();
// On calcule le nombre de page pour la pagination des articles
$pagesBookHome = ceil($countHome->id_count / 3);

// On récupère les articles
$getReadedBookHome = new bookReaded();

// Si il n'y a pas la variable $_GET['paReh'] ou qu'elle n'est pas inférieur ou égale à 0,
// la variable vaut automatiquement 0 et on affiche les 3 premiers articles
if (!isset($_GET['paReh']) || $_GET['paReh'] <= 0) {
    $displayReadedBookHome = $getReadedBookHome->getReadedBook(0);
// Si il y a bien une variable $_GET['paReh'],
// on attribut la valeur de $_GET['paReh'] à la variable $page
} else if (isset($_GET['paReh'])) {
    if (filter_var($_GET['paReh'], FILTER_VALIDATE_INT)) {
        $displayReadedBookHome = $getReadedBookHome->getReadedBook($_GET['paReh']);
    } else {
        header('Location: Accueil');
    }
}
?>