<?php
// On instancie un objet
$getUser = new user();

// Si il y a une session, la liste des bénévoles inscrit sur le site est récupérée pour être affiché
if (isset($_SESSION['id'])) {
    $displayGetUser = $getUser->getUser();
}

// On instancie un objet
$updateStatusUser = new user();
// On crée un tableau dans lequel il y a les messages destinés à l'utilisateur
$messageUpdateStatus = array();

// Si il y a une session
if (isset($_SESSION['id'])) {
    // Et que l'utilisateur clique sur le bouton pour changer le statut d'un bénévole
    if (isset($_GET['upUs']) && isset($_GET['idUs'])) {
        if (filter_var($_GET['idUs'], FILTER_VALIDATE_INT) && $_GET['upUs'] === 'Président' || 'Secrétaire' || 'Trésorier' || 'Bénévole') {
            $updateStatusUser->status_user = $_GET['upUs'];
            $updateStatusUser->id_user = $_GET['idUs'];
            // On modifie le statut de l'utilisateur
            $updateStatusUser->updateStatusUserById();

            if ($_GET['upUs'] === 'Président') {
                $updateStatusUser->status_user = 'Bénévole';
                $updateStatusUser->id_user = $_SESSION['id'];
                $updateStatusUser->updateStatusUserById();

                $_SESSION['status_user'] = 'Bénévole';
            }
        
        $messageUpdateStatus['updateStatus'] = 'Le statut du bénévole est bien modifié.';
        }
        
        // On recharge la page
        header('refresh:5; url=Profile');
    }
}