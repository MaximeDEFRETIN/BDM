<?php
// On instancie un objet
$validationComment = new comment_article();

if (isset($_GET['va'])) {
    if (filter_var($_GET['va'], FILTER_VALIDATE_INT)) {
        $validationComment->id_agdjjg_actuality = $_GET['va'];
        
        $commentValided = $validationComment->getCommentArticleById(1);
        $commentNoValided = $validationComment->getCommentArticleById(0);
        
    }
}

// On instancie un objet
$validated = new comment_article();

// Si l'utilisateur clique sur le bouton
if (isset($_GET['valCom']) && isset($_GET['va'])) {
    // On vérifie que $_POST['id'] et $_POST['id_actuality'] soient bien des entiers
    if (filter_var($_GET['valCom'], FILTER_VALIDATE_INT)) {
        if (filter_var($_GET['va'], FILTER_VALIDATE_INT)) {
            // On attribut les valeurs
            $validated->id = $_GET['valCom'];
            $validated->id_agdjjg_actuality = $_GET['va'];
            $validated->id_agdjjg_user = $_SESSION['id'];

            // On fait appel à la méthode
            $validated->validationComment(0);
            header('Location:Moderation-commentaire' . $_GET['va']);
        }
    }
}

// On instancie un objet
$unValidated = new comment_article();

// Si l'utilisateur clique sur le bouton
if (isset($_GET['uVaCom']) && isset($_GET['va'])) {
    // On vérifie que $_POST['id'] et $_POST['id_actuality'] soient bien des entiers
    if (filter_var($_GET['uVaCom'], FILTER_VALIDATE_INT) ) {
        if (filter_var($_GET['va'], FILTER_VALIDATE_INT)) {
            // On attribut les valeurs
            $unValidated->id = $_GET['uVaCom'];
            $unValidated->id_agdjjg_actuality = $_GET['va'];
            $unValidated->id_agdjjg_user = $_SESSION['id'];

            // On fait appel à la méthode
            $unValidated->validationComment(1);
            header('Location:Moderation-commentaire' . $_GET['va']);
        }
    }
}

// On instancie un objet
$deleteComment = new comment_article();

// Si l'utilisateur clique sur le bouton
if (isset($_GET['delCo']) && isset($_GET['va'])) {
    // On vérifie que $_POST['id'] et $_POST['id_actuality'] soient bien des entiers
    if (filter_var($_GET['delCo'], FILTER_VALIDATE_INT) ) {
        if (filter_var($_GET['va'], FILTER_VALIDATE_INT)) {
            // On attribut les valeurs
            $deleteComment->id = $_GET['delCo'];

            // On fait appel à la méthode
            $deleteComment->deleteComment();
            header('Location:Moderation-commentaire' . $_GET['va']);
        }
    }
}
?>