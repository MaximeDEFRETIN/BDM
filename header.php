<?php
// Permet de démarrer une session
session_start();

// On définit la durée d'une session en seconde, la session dure 24 minutes
$timeout_duration = 1430;

// Si il y a bien une activité et qu'il n'y a pas eu de requête avant l'expiration de la session,
// alors on redirige l'utilisateur vers la page d'accueil
if (isset($_SESSION['LAST_ACTIVITY']) && ($_SERVER['REQUEST_TIME'] - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
    session_unset();
    session_destroy();
    header('Location: Accueil');
}

// On réactualise le timestamp de la dernière requête de l'utilisateur
$_SESSION['LAST_ACTIVITY'] = $_SERVER['REQUEST_TIME'];

// include_once permet d'inclure les models et les contrôleurs si ils n'ont pas déjà été inclus
// On inclus les models et les contrôleurs
include_once 'models/dataBase.php';
include_once 'models/user.php';
include_once 'models/avatar.php';
include_once 'models/task.php';
include_once 'models/actuality.php';
include_once 'models/event_association.php';
include_once 'models/comment_article.php';
include_once 'models/bookReaded.php';
include_once 'controllers/displayedAvatar-Controller.php';
include_once 'controllers/event-Controller.php';
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/materialize.min.css" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="assets/js/checkMailUnique.js" type="text/javascript"></script>
        <script src="assets/js/script.js" type="text/javascript"></script>
        <link rel="stylesheet" href="assets/css/style.css" />
        <title>Bibliothèque des Malades</title>
    </head>
    <body class="row">
        <header>
            <nav role = "navigation" class="valign-wrapper">
                <div class="col s2 valign-wrapper">
                    <img src="<?= (isset($avatarDisplayed->path_avatar)?$avatarDisplayed->path_avatar:"assets/img/imgDefaultSmall.png") ?>"  height="50" width="50" class="circle responsive-img" id="avatarHeader" title="Avatar" alt="Avatar de <?= $_SESSION['first_name'] . " " . $_SESSION['last_name'] ?>" />
                    <p><?= $_SESSION['first_name'] . " " . $_SESSION['last_name'] ?></p>
                </div>
                <p class="col s2 offset-s4 center-align" title="Profile"><a href="Profile">Profile</a></p>
                <p class="col s2 center-align" title="Modification du profil"><a href="Modification-profile">Modifier ton profil</a></p>
                <p class="col s2 center-align" title="Déconnexion"><a href="controllers/deconnection-Controller.php">Déconnexion</a></p>
            </nav>
        </header>