<?php
// include_once permet d'inclure les models et les contrôleurs si ils n'ont pas déjà été inclus
// On inclus les models
include_once 'models/dataBase.php';
include_once 'models/user.php';
include_once 'models/actuality.php';
include_once 'models/comment_article.php';
include_once 'models/bookReaded.php';
include_once 'controllers/connection-Controller.php';
include_once 'controllers/mailRecoveryPassword-Controller.php';
include_once 'controllers/actualityHome-Controller.php';
include_once 'controllers/commentArticle-Controller.php';
include_once 'controllers/readedBookHome-Controller.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site de l'association Bibliothèque des Malades" />
        <link rel="stylesheet" href="../assets/css/materialize.min.css" />
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/script.js" type="text/javascript"></script>
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css"/>
        <title>Bibliothèque des Malades</title>
    </head>
    <body class="row lime lighten-5">
        <?php
        $messageHome = array($connectionMessage, $messageRecovery);
        foreach ($messageHome as $simpleMessage) { ?>
            <p class="center-align"><?= implode($simpleMessage) ?></p>
        <?php } ?>