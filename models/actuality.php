<?php
class actuality extends dataBase {

    public $article = '';
    public $title = '';
    public $id_agdjjg_user = 0;
    public $idCount = 0;
    public $id = 0;

    public function __construct() {
        parent::__construct();
    }

    /**
     * Permet d'ajouter un article
     */
    public function addActuality() {
        // On insère les un nouvel utilisateur
        $queryAddActuality = 'INSERT INTO `'.self::prefix.'actuality`(`article`, `title`, `date_actuality`, `id_agdjjg_user`) '
                            . 'VALUES (:article, :title,  CURDATE(), :id)';
        // On prépare la requête
        $requestAddActuality = $this->db->prepare($queryAddActuality);
        // Avec bindValue on associe le paramètre à la valeur à associer et on indique le type de valeur.
        // PDO:: est une constante
        $requestAddActuality->bindValue(':article', $this->article, PDO::PARAM_STR);
        $requestAddActuality->bindValue(':title', $this->title, PDO::PARAM_STR);
        $requestAddActuality->bindValue(':id', $this->id_agdjjg_user, PDO::PARAM_INT);
        // On exécute la requête
        return $requestAddActuality->execute();
    }

    /**
     * Permet de connaitre le nombre d'article écris
     */
    public function countArticle() {
        $queryCountArticle = 'SELECT COUNT(`id`) AS `idCount` FROM `'.self::prefix.'actuality` WHERE `id_agdjjg_user` IS NOT NULL';
        // On prépare la requête
        $requestCountArticle = $this->db->prepare($queryCountArticle);
        // Si la requête est exécutée
        if ($requestCountArticle->execute()) {
            return $countArticle = $requestCountArticle->fetch(PDO::FETCH_OBJ);
        }
    }

    /**
     * Permet de récupérer les articles pour les afficher sur la page profile
     * @$page -> variable correspondant à la pagination
     */
    public function getArticle($page) {
        $limit = 3;
        $offset = $limit * $page;

        $queryGetActuality = 'SELECT `'.self::prefix.'actuality`.`id` AS `id`, `last_name`, `first_name`, `article`,`title`, DATE_FORMAT(`date_actuality`, "%d/%m/%Y") AS `date_actuality` '
                                . 'FROM `'.self::prefix.'actuality` '
                           . 'INNER JOIN `'.self::prefix.'user` '
                                . 'ON `'.self::prefix.'user`.`id` = `'.self::prefix.'actuality`.`id_agdjjg_user` '
                             . 'WHERE `id_agdjjg_user` IS NOT NULL OR `id_agdjjg_user` IS NULL '
                           . 'LIMIT ' . $limit . ' OFFSET ' . $offset;
        // On prépare la requête
        $requestGetActuality = $this->db->prepare($queryGetActuality);
        // Si la requête est exécutée
        if ($requestGetActuality->execute()) {
            // Et que la requête est un objet
            if (is_object($requestGetActuality)) {
                // On retourne le résultat sous forme d'un tableau
                return $requestGetActualityResult = $requestGetActuality->fetchAll(PDO::FETCH_OBJ);
            }
        }
    }

    /**
     * Permet de récupérer un article par son id pour l'afficher aux visiteurs
     */
    public function getArticleById() {
        $queryGetArticle = 'SELECT `'.self::prefix.'actuality`.`id` AS `id`, `id_agdjjg_user`, `last_name`, `first_name`, `article`, `title`, DATE_FORMAT(`date_actuality`, "%d/%m/%Y") AS `date_actuality` '
                            . 'FROM `'.self::prefix.'actuality` '
                         . 'INNER JOIN `'.self::prefix.'user` '
                                . 'ON `'.self::prefix.'user`.`id` = `'.self::prefix.'actuality`.`id_agdjjg_user` '
                         . 'WHERE `'.self::prefix.'actuality`.`id` = :id';
        // On prépare la requête
        $requestGetArticle = $this->db->prepare($queryGetArticle);
        // Avec bindValue on associe le paramètre à la valeur à associer et on indique le type de valeur.
        // PDO:: est une constante
        $requestGetArticle->bindValue(':id', $this->id, PDO::PARAM_INT);
        // Si la requête est exécutée
        if ($requestGetArticle->execute()) {
            // Et que la requête est un objet
            if (is_object($requestGetArticle)) {
                // On retourne le résultat sous forme d'un tableau
                return $requestGetArticleResult = $requestGetArticle->fetchAll(PDO::FETCH_OBJ);
            }
        }
    }

    /**
     * Permet de modifier un article
     */
    public function updateArticleById() {
        $queryUpdateArticle = 'UPDATE `'.self::prefix.'actuality` '
                                . 'SET `article`= :article,`title`= :title '
                            . 'WHERE `id` = :id';
        // On prépare la requête
        $requestUpdateArticle = $this->db->prepare($queryUpdateArticle);
        // Avec bindValue on associe le paramètre à la valeur à associer et on indique le type de valeur.
        // PDO:: est une constante
        $requestUpdateArticle->bindValue(':id', $this->id, PDO::PARAM_INT);
        $requestUpdateArticle->bindValue(':title', $this->title, PDO::PARAM_STR);
        $requestUpdateArticle->bindValue(':article', $this->article, PDO::PARAM_STR);
        // On exécute la requête
        return $requestUpdateArticle->execute();
    }

    /**
     * Permet de supprimer une tâche
     */
    public function deleteActualityById() {
        // On supprime un article
        $queryDeleteCommentActuality = 'DELETE FROM `'.self::prefix.'actuality` WHERE `id` = :id';
        // On prépare la requête
        $requestDeleteCommentActuality = $this->db->prepare($queryDeleteCommentActuality);
        // Avec bindValue on associe le paramètre à la valeur à associer et on indique le type de valeur.
        // PDO:: est une constante
        $requestDeleteCommentActuality->bindValue(':id', $this->id, PDO::PARAM_INT);
        // On exécute la requête
        return $requestDeleteCommentActuality->execute();
    }

    public function __destruct() {
        parent::__destruct();
    }
}
