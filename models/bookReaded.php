<?php
class bookReaded extends dataBase {
    
    public $id = 0;
    public $title = '';
    public $opinion_book = '';
    public $date_edited = '';
    public $id_agdjjg_user = 0;
    
    public function addBookReaded() {
        // On insère la descecription d'un livre lu
        $queryAddReaded = 'INSERT INTO `'.self::prefix.'bookReaded`(`title`, `opinion_book`, `date_edited`, `id_agdjjg_user`) '
                            . 'VALUES (:title, :opinion_book, CURDATE(), (SELECT `id` FROM `'.self::prefix.'user` WHERE `mail` = :mail))';
        $requestAddReaded = $this->db->prepare($queryAddReaded);
        //Avec bindValue, on associe le paramètre à la valeur à associer et on indique le type de valeur. PDO:: est une constante
        $requestAddReaded->bindValue(':title', $this->title, PDO::PARAM_STR);
        $requestAddReaded->bindValue(':opinion_book', $this->opinion_book, PDO::PARAM_STR);
        $requestAddReaded->bindValue(':mail', $this->mail, PDO::PARAM_STR);
        // On éxécute la requête
        return $requestAddReaded->execute();
    }
    
    /**
     * Permet de connaitre le nombre d'article écris
     */
    public function countReadedBook() {
        // On compte le nombre de description de livre
        $queryCountReaded = 'SELECT COUNT(`id`) AS `id_count` FROM `'.self::prefix.'bookReaded` WHERE `id_agdjjg_user` IS NOT NULL';
        // On prépare la requête
        $requestCountReaded = $this->db->prepare($queryCountReaded);
        // Si la requête est exécutée
        if ($requestCountReaded->execute()) {
            // On retourne le résultat
            return $countReaded = $requestCountReaded->fetch(PDO::FETCH_OBJ);
        }
    }
    
    /**
     * Permet de récupérer toutes les descriptions de livre
     */
    public function getReadedBook($page) {
        $limit = 3;
        $offset = $limit * $page;
    
        $queryGetOpinion = 'SELECT `'.self::prefix.'bookReaded`.`id` AS `id`, `title`, `first_name`, `last_name`, `opinion_book`, `id_agdjjg_user`, DATE_FORMAT(`date_edited`, "%d/%m/%Y") AS `date_edited` '
                                . 'FROM `'.self::prefix.'bookReaded` '
                         . 'INNER JOIN `'.self::prefix.'user` '
                                . 'ON `'.self::prefix.'user`.`id` = `'.self::prefix.'bookReaded`.`id_agdjjg_user` '
                         . 'LIMIT ' . $limit . ' OFFSET ' . $offset;
        // On prépare la requête
        $requestGetOpinion = $this->db->prepare($queryGetOpinion);
        // Si la requête est exécutée
        if ($requestGetOpinion->execute()) {
            // Et que la requête est un objet
            if (is_object($requestGetOpinion)) {
                // On retourne le résultat sous forme d'un tableau
                return $requestGetOpinionResult = $requestGetOpinion->fetchAll(PDO::FETCH_OBJ);
            }
        }
    }
    
    /**
     * Permet de sélectionner une description d'un livre en fonction de son id
     */
    public function getReadedById() {
        $queryGetReaded = 'SELECT `title`, `first_name`, `last_name`, `opinion_book`, DATE_FORMAT(`date_edited`, "%d/%m/%Y") AS `date_edited` '
                                . 'FROM `'.self::prefix.'bookReaded` '
                         . 'INNER JOIN `'.self::prefix.'user` '
                                . 'ON `'.self::prefix.'user`.`id` = `'.self::prefix.'bookReaded`.`id_agdjjg_user` '
                         . 'WHERE `'.self::prefix.'bookReaded`.`id` = :id';
        // On prépare la requête
        $requestGetReaded = $this->db->prepare($queryGetReaded);
        // Avec bindValue on associe le paramètre à la valeur à associer et on indique le type de valeur.
        // PDO:: est une constante
        $requestGetReaded->bindValue(':id', $this->id, PDO::PARAM_INT);
        // Si la requête est exécutée
        if ($requestGetReaded->execute()) {
            // Et que la requête est un objet
            if (is_object($requestGetReaded)) {
                // On retourne le résultat sous forme d'un tableau
                return $requestGetReadedResult = $requestGetReaded->fetchAll(PDO::FETCH_OBJ);
            }
        }
    }
    
    /**
     * Permet à l'utilsiateur de modifier la description du livre
     */
    public function updateReadedBook() {
        // On modifie les informations liées au profil d'un utilisateur en fonction de son id
        $queryUpdateReadedBook = 'UPDATE `'.self::prefix.'bookReaded` SET `title` = :title, `opinion_book` = :opinion_book '
                                . 'WHERE `id` = :id';
        // On prépare la requête
        $requestUpdateProfil = $this->db->prepare($queryUpdateReadedBook);
        // Avec bindValue on associe le paramètre à la valeur à associer et on indique le type de valeur. PDO:: est une constante
        $requestUpdateProfil->bindValue(':id', $this->id, PDO::PARAM_INT);
        $requestUpdateProfil->bindValue(':title', $this->title, PDO::PARAM_STR);
        $requestUpdateProfil->bindValue(':opinion_book', $this->opinion_book, PDO::PARAM_STR);
        // On éxécute la requête
        return $requestUpdateProfil->execute();
    }

    /**
     * Permet à un utilisateur de supprimer la description d'un livre
     */
    public function deleteReadedBookById() {
        // On supprime l'utilisateur en fonction de son id
        $queryDeleteProfil = 'DELETE FROM `'.self::prefix.'bookReaded` WHERE `id` = :id';
        // On prépare la requête
        $requestDeleteProfil = $this->db->prepare($queryDeleteProfil);
        $requestDeleteProfil->bindValue(':id', $this->id, PDO::PARAM_INT);
        // On exécute la requête
        return $requestDeleteProfil->execute();
    }
}
?>