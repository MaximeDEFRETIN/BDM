<?php
include_once 'header.php';

include_once 'controllers/addUser-Controller.php';
include_once 'controllers/addTask-Controller.php';
include_once 'controllers/displayTask-Controller.php';
include_once 'controllers/addVolunteer-Controller.php';
include_once 'controllers/user-Controller.php';
include_once 'controllers/actualityProfile-Controller.php';
include_once 'controllers/readedBook-Controller.php';
include_once 'controllers/readedHome-Controller.php';
?>
<?php
$message = array($messageReaded, $messageArticle, $messageSignUp, $messageTask, $messageStatusTask, $messageAddVolunteer, $messageUpdateStatus, $messageEvent, $messageVolunteerEvent, $messageDeleteVolunteerTask, $messageDeletVolunteerEvent);
    foreach ($message as $simpleMessage) { ?>
        <p class="center-align "><?= implode($simpleMessage) ?></p>
<?php } ?>
<div class="row marginTopMin">
    <div class="row">
        <table class="highlight responsive-table col s10 offset-s1 centered">
            <thead>
                <tr>
                    <th>Titre</th>
                    <th>Actualité de l'association</th>
                    <th>Auteur(e)</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($displayActualityProfile as $display) { ?>
                    <tr>
                        <td class="justify"><?= $display->title ?></td>
                        <td class="justify"><?= $display->article ?></td>
                        <td><?= $display->first_name ?> <?= $display->last_name ?></td>
                        <td><time datetime="<?= $display->date_actuality ?>"><?= $display->date_actuality ?></time></td>
                        <?php if ($_SESSION['status_user'] === 'Président') { ?>
                            <td><a class="btn col s12" href="Moderation-commentaire<?= $display->id ?>" title="Modérer les commentaiers reçus"><i class="small material-icons">thumbs_up_down</i></a></td>
                            <td><a class="btn col s12" href="Changement-article-<?= $display->id ?>" title="Modifier une actualité"><i class="small material-icons">error</i></a></td>
                            <td><a class="btn col s12" href="Suppression-actualite-<?= $display->id ?>" title="Supprimer une actualité"><i class="small material-icons">close</i></a></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <ul class="pagination center-align">
            <?php for($i = 0; $i < $pages; $i++) { ?>
                <li>
                    <a href="Page-actualité-<?= $i ?>"><?= $i + 1 ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <?php if ($_SESSION['status_user'] === 'Président') { ?>
        <div class="row marginTopMin">
            <button data-target="article" class="btn modal-trigger col s4 offset-s4 marginBottomMin" title="Écrire un article">Écrire un article</button>
            <div id="article" class="modal">
                <div class="modal-content">
                    <h2 class="center-align">Écrire l'article</h2>
                    <form name="writingArticle" id="writingArticle" method="POST">
                        <div class="input-field col s12">
                            <input type="text" id="titleArticle" name="titleArticle" class="validate" maxlength="25" data-length="25" title="Titre" />
                            <label for="title" class="black-text">Titre de l'article</label>
                        </div>
                        <div class="input-field col s12">
                            <textarea id="article" name="article" class="col s12 materialize-textarea" title="Zone de texte"></textarea>
                            <label for="article" class="black-text">Article</label>
                        </div>
                        <input type="submit" id="submitArticle" name="submitArticle" class="btn col s6 offset-s3 marginBottomMin" value="Écrire" title="Envoie l'article" />
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<div class="row marginTopMin">
    <div class="row">
        <table class="highlight responsive-table col s10 offset-s1 centered">
            <thead>
                <tr>
                    <th>Titre du livre</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Auteur(e)</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($displayReadedBook as $display) { ?>
                    <tr>
                        <td class="justify"><?= $display->title ?></td>
                        <td><?= $display->date_edited ?></td>
                        <td class="justify"><?= $display->opinion_book ?></td>
                        <td><?= $display->first_name ?> <?= $display->last_name ?></td>
                        <?php if ($display->id_agdjjg_user === $_SESSION['id']) { ?>
                            <td><a href="Changement-lecture-<?= $display->id ?>" class="btn col s12" title="Changer la description du livre"><i class="small material-icons">error</i></a></td>
                            <td><a href="profile.php?delRea=<?= $display->id ?>" class="btn col s12" title="Supprimer la description du livre"><i class="small material-icons">close</i></a></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <ul class="pagination center-align">
            <?php for($i = 0; $i < $pagesBook; $i++) { ?>
                <li>
                    <a href="Page-profile-<?= $i ?>"><?= $i + 1 ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="row">
        <button data-target="readedBook" class="btn modal-trigger col s4 offset-s4 marginBottomMin" title="Écrire un article">Parler d'un livre</button>
        <div id="readedBook" class="modal">
            <div class="modal-content">
                <h2 class="center-align">Le livre que tu as lu ...</h2>
                <form method="POST">
                    <div class="input-field col s12">
                        <input type="text" id="title" name="title" class="validate" maxlength="255" data-length="255" title="Titre" />
                        <label for="title" class="black-text">Titre du livre</label>
                    </div>
                    <div class="input-field col s12">
                        <textarea id="descriptionBook" name="descriptionBook" class="col s12 materialize-textarea" title="Décris le ivre que tu as lu"></textarea>
                        <label for="descriptionBook" class="black-text">Description du livre</label>
                    </div>
                    <input type="submit" id="submitDescription" name="submitDescription" class="btn col s6 offset-s3 marginBottomMin" value="Écrire" title="Envoie la description" />
                </form>
            </div>
        </div>
    </div>
</div>
<?php if ($_SESSION['status_user'] !== 'Bénévole') { ?>
    <div class="row marginTopMin">
        <?php if ($_SESSION['status_user'] === 'Président') { ?>
            <div class="col s10 offset-s1">
                <button data-target="volunteerRegistration" class="btn modal-trigger col s10 offset-s1 marginTopMin" title="Ajouter un bénévole">Ajouter un bénévole</button>
                <div id="volunteerRegistration" class="modal">
                    <div class="modal-content">
                        <h2 class="center-align">Informations sur le bénévole</h2>
                        <form name="volunteerIdentity" id="volunteerIdentity" method="POST" action="">
                            <div class="col s12 input-field">
                                <input type="text" name="last_name" id="last_name" class="validate" maxlength="255" data-length="255" title="Nom" />
                                <label for="last_name" class="black-text">Nom</label>
                            </div>
                            <div class="marginTop col s12 input-field">
                                <input type="text" name="first_name" id="first_name" class="validate" maxlength="255" data-length="255" title="Prénom" />
                                <label for="first_name" class="black-text">Prénom</label>
                            </div>
                            <div class="col s12 input-field inline">
                                <input type="email" name="mail" id="mail" class="validate" maxlength="255" data-length="255" title="Mail" onblur="checkMailUnique()" />
                                <label for="mail" data-error="Adresse mail faussement écris." data-success="Adresse mail correctement écris." class="black-text">Adresse mail</label>
                                <span id="errorCheckMailUnique">Cette addresse mail est déjà utilisée.</span>
                            </div>
                            <input type="submit" id="submitRegistrer" name="submitRegistrer" class="btn col s6 offset-s3 marginBottomMin" value="Enregistrer le bénévole" title="Enregistrer le bénévole" />
                        </form>
                    </div>
                </div>
            </div>
        <?php }?>
        <div class="row">
            <table class="highlight responsive-table col s10 offset-s1 centered center-align">
                <thead>
                    <tr>
                        <th>Nom et prénom du bénévole</th>
                        <th>Statut du bénévole</th>
                        <th>Mail</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($displayGetUser as $display) { ?>
                        <tr>
                            <td><?= $display->first_name . ' ' . $display->last_name ?></td>
                            <td><?= $display->status_user ?></td>
                            <td><?= $display->mail ?></td>
                        </tr>
                        <tr>
                            <?php if ($_SESSION['status_user'] === 'Président') { ?>
                                <td>
                                    <a class="btn <?= ($display->status_user === 'Président')?'disabled':'' ?>" href="Profile-status-President-utilisateur-<?= $display->id_user ?>" title="Président"><i class="tiny material-icons">face</i></a>
                                </td>
                                <td>
                                    <a class="btn <?= ($display->status_user === 'Secrétaire')?'disabled':'' ?>" href="Profile-status-Secretaire-utilisateur-<?= $display->id_user?>" title="Secrétaire"><i class="tiny material-icons">local_library</i></a>
                                </td>
                                <td>
                                    <a class="btn <?= ($display->status_user === 'Trésorier')?'disabled':'' ?>" href="Profile-status-Tresorier-utilisateur-<?= $display->id_user ?>" title="Trésorier"><i class="tiny material-icons">attach_money</i></a>
                                </td>
                                <td>
                                    <a class="btn <?= ($display->status_user === 'Bénévole')?'disabled':'' ?>" href="Profile-status-Benevole-utilisateur-<?= $display->id_user ?>" title="Bénévole"><i class="tiny material-icons">tag_faces</i></a>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php } ?>
<div class="row col s10 offset-s1 marginTopMin">
    <button data-target="taskEdit" class="btn modal-trigger col s10 offset-s1 marginBottomMin" title="Ajouter une tâche">Ajouter une tâche à réaliser</button>
    <div id="taskEdit" class="modal">
        <div class="modal-content">
            <h2 class="center-align">Éditer la tâche</h2>
            <form name="formTask" id="formTask" method="POST">
                <div class="marginTop col s12 input-field">
                    <input type="text" name="suggestedTask" id="suggestedTask" class="validate" title="Tâche suggérée" maxlength="255" data-length="255" />
                    <label for="suggestedTask" class="black-text">Tâche suggérée</label>
                </div>
                <div class="input-field col s12">
                    <textarea id="descriptionTask" name="descriptionTask" class="col s12 materialize-textarea" title="Description" maxlength="255" data-length="255"></textarea>
                    <label for="descriptionTask" class="black-text">Description de la tâche</label>
                </div>
                <input type="submit" id="submitTask" name="submitTask" class="btn col s6 offset-s3 marginBottomMin" value="Proposer une nouvelle tâche" title="Nouvellle tâche" />
            </form>
        </div>
    </div>
    <table class="highlight responsive-table col s10 offset-s1 centered">
        <thead>
            <tr>
                <th>Tâche proposée</th>
                <th>Description</th>
                <th>Statut de la tâche</th>
                <th>Auteur</th>
                <th>Date</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($displayTask as $display) { ?>
                <tr>
                    <td class="justify"><?= $display->suggested_task ?></td>
                    <td class="justify"><?= $display->description_task ?></td>
                    <td><?= $display->status_task ?></td>
                    <td><?= $display->first_name ?> <?= $display->last_name ?></td>
                    <td><time datetime="<?= $display->date_task ?>"><?= $display->date_task ?></time></td>
                    <?php if ($display->status_task !== 'Terminée') { ?>
                        <td><?php if ($display->id_agdjjg_user === $_SESSION['id']) { ?><a class="btn" href="profile.php?delTas=<?= $display->id_task ?>" title="Supprimer une tâche"><i class="small material-icons">close</i></a><?php } ?>
                            <a class="btn" href="Profile-ajout-Volontaire-<?= $display->id_task ?>" title="Ajouter un volontaire"><i class="small material-icons">account_circle</i></a>
                            <a class="btn" href="Changement-status-<?= $display->id_task ?>" title="Changement de statut"><i class="small material-icons">check</i></a>
                            <?php if ($display->id_agdjjg_user === $_SESSION['id']) { ?><a class="btn" href="Changement-tache-<?= $display->id_task ?>" title="Modifier une tâche"><i class="small material-icons">error</i></a><?php } ?>
                            <a class="btn" href="Désistement-<?= $display->id_task ?>" title="Se délister de la tâche"><i class="small material-icons">directions_run</i></a></td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <table class="highlight responsive-table col s10 offset-s1 centered">
        <thead>
            <tr>
                <th>Tâche suggégérée en cours</th>
                <th>Bénévole y partitcipant</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($displayAssignedVolunter as $display) { ?>
                <tr>
                    <?php if ($display->status_task !== 'Terminée') { ?>
                        <td><?= $display->suggested_task ?></td>
                        <td><?= $display->volunteer ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div class="row col offset-s1 s10 marginTopMin">
    <button data-target="eventEdit" class="btn modal-trigger col s10 offset-s1 marginBottomMin" title="Créer un évènnement">Créer un évènnement</button>
    <div id="eventEdit" class="modal">
        <div class="modal-content">
            <h2 class="center-align">Détails de l'évènnement</h2>
            <form name="formEvent" id="formEvent" method="POST" enctype="multipart/form-data">
                <div class="col s12 input-field">
                    <input type="text" name="suggestedEvent" id="suggestedEvent" class="validate" maxlength="255" data-length="255" title="Évènement suggérée" />
                    <label for="suggestedEvent" class="black-text">Évènnement suggéré</label>
                </div>
                <label for="choiceStatus" class="black-text">Statut de l'évènnement</label>
                <select name="statusEvent" class="browser-default" title="Choix du type d'événement">
                    <option disabled selected>Choisie un statut</option>
                    <option value="Formation">Formation</option>
                    <option value="Réunion">Réunion</option>
                    <option value="Entre bénévole">Entre bénévole</option>
                    <option value="Sortie">Sortie</option>
                    <option value="Autres">Autres</option>
                </select>
                <div class="col s12">
                    <label for="dateEvent" class="black-text">Date de l'évennement</label>
                    <input type="date" id="dateEvent" class="datepicker" name="dateEvent" value="<?= date('Y-m-d') ?>" title="Date" />
                </div>
                <div class="input-field col s12">
                    <textarea id="descriptionEvent" name="descriptionEvent" class="materialize-textarea" maxlength="2((" data-length="2((" title="Description de l'évènement"></textarea>
                    <label for="descriptionEvent" class="black-text">Description de l'évènnements</label>
                </div>
                <input type="button" id="addDocumentEvent" class="btn col s6 offset-s3 marginBottomMin" value="Veux-tu ajouter un document ?" title="Veux-tu ajouter un document ?" />
                <div class="file-field input-field col s12" id="displayInputFileEvent">
                    <div class="btn">
                        <span>Document</span>
                        <input type="file" id="documentFileEvent" name="documentFileEvent" accept=".pdf" />
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>
                <input type="submit" id="submitEvent" name="submitEvent" class="btn col s6 offset-s3 marginBottomMin" value="Proposer l'évènnement" title="Proposer l'évènement" />
            </form>
        </div>
    </div>
    <table class="highlight responsive-table col s10 offset-s1 centered marginTopMin">
        <thead>
            <tr>
                <th>Évènnement</th>
                <th>Description</th>
                <th>Statut</th>
                <th>Auteur</th>
                <th>Date</th>
                <th>Document</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($displayEvent as $display) { ?>
                <tr>
                    <td class="justify"><?= $display->suggested_event ?></td>
                    <td class="justify"><?= $display->description_event ?></td>
                    <td><?= $display->status_event ?></td>
                    <td><?= $display->first_name ?> <?= $display->last_name ?></td>
                    <td><time datetime="<?= $display->date_event ?>"><?= $display->date_event ?></time></td>
                    <td><?php if($display->path_document_event) { ?><a href="<?= $display->path_document_event ?>" target="_blank" title="<?= basename($display->path_document_event) ?>"><?= basename($display->path_document_event) ?></a><?php } ?></td>
                </tr>
                <tr>
                    <td>
                        <a class="btn col s12" href="Profile-ajout-participant-<?= $display->id_event_association ?>" title="Ajouter un participant"><i class="small material-icons">account_circle</i></a>
                    </td>
                    <td>
                        <a class="btn col s12" href="profile.php?idEvent=<?= $display->id_event_association ?>" title="Se désister"><i class="small material-icons">directions_run</i></a>
                    </td>
                    <td>
                        <?php if ($display->id_agdjjg_user === $_SESSION['id']) { ?><a class="btn" href="Changement-evenement-<?= $display->id_event_association ?><?= ($display->path_document_event)?'-fichier-'.basename($display->path_document_event):'' ?>" title="Modifier"><i class="tiny material-icons">error</i></a><?php } ?>
                    </td>
                    <td>
                        <?php if ($display->id_agdjjg_user === $_SESSION['id']) { ?><a class="btn" href="profile.php?delEv=<?= $display->id_event_association ?><?= ($display->path_document_event)?'&path='.basename($display->path_document_event):'' ?>" title="Supprimer"><i class="tiny material-icons">close</i></a><?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <table class="highlight responsive-table col s10 offset-s1 centered marginTopMin">
        <thead>
            <tr>
                <th>Évènement prochain</th>
                <th>Bénévole y partitcipant</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($displayVolunteerEvent as $display) { ?>
                <tr>
                    <td class="justify"><?= $display->suggested_event ?></td>
                    <td><?= $display->volunteer_present ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php include_once 'footer.php';