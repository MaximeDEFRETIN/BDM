<?php
require_once 'headerHome.php';

require_once 'controllers/readedHome-Controller.php';
?>
<div class="fixed-action-btn"><a href="Accueil" class="btn" title="Accueil">Accueil</a></div>
<?php foreach($displayReaded as $display) { ?>
    <article title="Article - <?= $display->title ?>" class="col s6 offset-s3 marginBottomMin marginTopMin article">
        <h4><?= $display->title ?></h4>
        <p class="right-align">écrit le <time class="italic" datetime="<?= $display->date_edited ?>"><?= $display->date_edited ?> par <span class="auteur"><?= $display->first_name ?> <?= $display->last_name ?></span></time></p>
        <p class="justify"><?= $display->opinion_book ?></p>
    </article>
<?php } ?>
<?php include_once 'footerHome.php';